package com.alex.cvprocess.web;

import com.alex.cvprocess.domain.UserAccount;
import com.alex.cvprocess.dto.CVDto;
import com.alex.cvprocess.dto.CVProcessDto;
import com.alex.cvprocess.dto.DocumentDto;
import com.alex.cvprocess.service.CVService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.ContentDisposition.builder;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@RestController
@RequestMapping("/api/rest/cv")
@RequiredArgsConstructor
public class CVController {
    private final CVService cvService;

    /*
        CRUD для работы с CV
     */

    @PostMapping
    public Long create(@AuthenticationPrincipal UserAccount userAccount) {
        return cvService.createCV(userAccount.getId());
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<?> delete(@PathVariable Long id) {
        cvService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<CVDto> get(@AuthenticationPrincipal UserAccount userAccount, @PathVariable long id) {
        CVDto cv = cvService.findCVById(userAccount.getId(), id);
        if (cv == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(cv);
    }

    /*
        Эндпоинты для продвижения CV по flow. Каждый эндпоинт может вызываться с определенным статусом
        резюме и переключает резюме в следующий статус
     */

    @PostMapping("/{id}/change-template")
    public ResponseEntity<?> changeTemplate(@PathVariable Long id) {
        cvService.changeTemplate(id);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/choose-template")
    public ResponseEntity<?> chooseTemplate(@PathVariable Long id, @RequestBody Long templateId) {
        cvService.updateTemplate(id, templateId);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("/{id}/update-data")
    public ResponseEntity<?> updateData(@PathVariable Long id, @RequestBody Map<String, Object> data) {
        cvService.updateData(id, data);
        return ResponseEntity.noContent().build();
    }

    @PostMapping("/{id}/submit")
    public ResponseEntity<?> submit(@PathVariable Long id, @RequestBody Map<String, Object> attributes) {
        cvService.submit(id, attributes);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/user")
    public List<CVProcessDto> getAllByUserAccount(@AuthenticationPrincipal UserAccount userAccount) {
        return cvService.findCVByUserAccount(userAccount.getId());
    }

    @GetMapping("/{id}/document")
    public ResponseEntity<byte[]> getDocument(@AuthenticationPrincipal UserAccount userAccount, @PathVariable Long id) {
        DocumentDto documentDto = cvService.getCVDocument(userAccount.getId(), id);

        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, builder("attachment").filename(documentDto.getFileName(), UTF_8).build().toString())
                .header(CONTENT_TYPE, documentDto.getMediaType())
                .body(documentDto.getContent());
    }
}
