package com.alex.cvprocess.web.filters;

import com.alex.cvprocess.config.auth.AuthenticationProvider;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

import static org.springframework.util.StringUtils.hasText;

/**
 * This filter monitors all requests,
 * validates JWT token if needed and sets principal to SecurityContext
 */
@Component
public class JwtFilter extends GenericFilterBean {

    private static final String AUTHORIZATION = "Authorization";
    private static final String BEARER_JWT_PREFIX = "Bearer ";

    private final AuthenticationProvider authenticationProvider;

    @Value("${security.system.token}")
    private String systemToken;

    public JwtFilter(AuthenticationProvider authenticationProvider) {
        this.authenticationProvider = authenticationProvider;
    }

    /**
     * This method to be invoked before each controller method invoke.
     * Then it takes JWT token from request header and validates it
     * If token is valid it extracts principal and sets it to security context
     * See {@link AuthenticationProvider#authenticate}
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String token = getTokenFromRequest((HttpServletRequest) servletRequest);
        authenticationProvider.authenticate(token);
        filterChain.doFilter(servletRequest, servletResponse);
    }

    private String getTokenFromRequest(HttpServletRequest request) {
        String bearer = request.getHeader(AUTHORIZATION);
        if (hasText(bearer) && bearer.startsWith(BEARER_JWT_PREFIX)) {
            return bearer.substring(BEARER_JWT_PREFIX.length());
        }
        return null;
    }

}