package com.alex.cvprocess.web;

import com.alex.cvprocess.domain.UserAccount;
import com.alex.cvprocess.dto.UserAccountDto;
import com.alex.cvprocess.dto.UserRegistrationDto;
import com.alex.cvprocess.service.UserAccountService;
import lombok.AllArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
@AllArgsConstructor
public class UserController {
    private final UserAccountService userAccountService;

    @PostMapping
    public Long createUser(@RequestBody UserRegistrationDto dto) {
        return userAccountService.addUser(dto);
    }

    @PreAuthorize("hasAuthority('SYSTEM')")
    @PostMapping(value = "/admins")
    public Long createAdmin(@RequestBody UserRegistrationDto dto) {
        return userAccountService.addUser(dto);
    }

    @GetMapping(value = "/me")
    public UserAccountDto me(@AuthenticationPrincipal UserAccount userAccount) {
        return UserAccountDto.of(userAccount);
    }

    @PreAuthorize("hasAuthority('SYSTEM')")
    @GetMapping("/func/try-find-by-username-and-password")
    public UserAccountDto tryFindUserByNameAndPassword(@RequestParam("username") String username,
                                                       @RequestParam("password") String password) {
        return userAccountService.findUserByNameAndPassword(username, password);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @GetMapping
    public List<UserAccountDto> findAll() {
        return userAccountService.findAll();
    }
}
