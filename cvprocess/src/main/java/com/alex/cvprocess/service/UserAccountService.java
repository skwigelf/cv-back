package com.alex.cvprocess.service;

import com.alex.cvprocess.dto.UserAccountDto;
import com.alex.cvprocess.dto.UserRegistrationDto;

import java.util.List;

public interface UserAccountService {
    Long addUser(UserRegistrationDto userDto);

    UserAccountDto findUserByNameAndPassword(String name, String password);

    List<UserAccountDto> findAll();
}
