package com.alex.cvprocess.service;

import com.alex.cvprocess.adapters.documents.DocumentCreatorAdapter;
import com.alex.cvprocess.domain.CV;
import com.alex.cvprocess.domain.Document;
import com.alex.cvprocess.domain.UserAccount;
import com.alex.cvprocess.dto.CVDto;
import com.alex.cvprocess.dto.CVProcessDto;
import com.alex.cvprocess.dto.DocumentDto;
import com.alex.cvprocess.dto.TemplateInfoDto;
import com.alex.cvprocess.exceptions.*;
import com.alex.cvprocess.repo.CVRepository;
import com.alex.cvprocess.repo.DocumentRepository;
import com.alex.cvprocess.repo.UserAccountRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.alex.cvprocess.domain.CVStatus.*;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;

@Service
@AllArgsConstructor
public class CVServiceImpl implements CVService {
    private final CVRepository cvRepository;
    private final UserAccountRepository userAccountRepository;
    private final DocumentRepository documentRepository;
    private final DocumentCreatorAdapter documentCreatorAdapter;
    private final ObjectMapper objectMapper = new ObjectMapper();

    @Transactional
    @Override
    public Long createCV(Long userId) {
        CV cv = new CV();
        UserAccount user = userAccountRepository.findById(userId).orElseThrow();

        cv.setStatus(NEW);
        cv.setCreatedDate(LocalDate.now());
        cv.setUserAccount(user);
        return cvRepository.save(cv).getId();
    }

    @Transactional
    @Override
    public CVDto findCVById(long userId, long id) {
        CV cv = cvRepository.findById(id).orElseThrow(CVNotFound::new);
        UserAccount user = userAccountRepository.findById(userId).orElseThrow(UserNotFound::new);
        assertCVBelongsToUser(user, cv);

        return mapToDto(cv);
    }

    @Transactional
    @Override
    public List<CVProcessDto> findCVByUserAccount(Long userId) {
        UserAccount user = userAccountRepository.findById(userId).orElseThrow(UserNotFound::new);

        List<CV> cvs = cvRepository.findByUserAccount(user);
        List<Long> templateIds = cvs.stream().map(CV::getTemplateId).collect(toList());
        List<TemplateInfoDto> templateInfos = documentCreatorAdapter.getTemplatesByIds(templateIds);
        Map<Long, TemplateInfoDto> templatesMap = templateInfos.stream().collect(toMap(TemplateInfoDto::getId, template -> template));
        return cvs.stream().map(cv -> mapToProcessDto(cv, templatesMap)).collect(toList());
    }

    @Transactional
    @Override
    public DocumentDto getCVDocument(long userAccId, long id) {
        CV cv = cvRepository.findById(id).orElseThrow(CVNotFound::new);
        UserAccount userAccount = userAccountRepository.findById(userAccId).orElseThrow(UserNotFound::new);
        assertCVBelongsToUser(userAccount, cv);

        return mapToDto(cv.getDocument());
    }

    @Transactional
    @Override
    public void delete(Long id) {
        cvRepository.deleteById(id);
    }

    @SneakyThrows
    @Transactional
    @Override
    public void updateData(Long id, Map<String, Object> data) {
        CV cv = cvRepository.findById(id).orElseThrow(CVNotFound::new);
        if (cv.getStatus() != FILL_DATA) {
            throw new IllegalCVStatus("Invalid operation");
        }
        String jsonData = objectMapper.writeValueAsString(data);
        cv.setData(jsonData);
    }

    @SneakyThrows
    @Transactional
    @Override
    public void submit(Long id, Map<String, Object> data) {
        String jsonData = objectMapper.writeValueAsString(data);
        CV cv = cvRepository.findById(id).orElseThrow();
        if (cv.getStatus() != FILL_DATA) {
            throw new IllegalCVStatus("Invalid operation");
        }
        DocumentDto docDto = documentCreatorAdapter.createCV(cv.getTemplateId(), data);
        Document document = documentRepository.save(mapFromDto(docDto));
        cv.setDocument(document);
        cv.setData(jsonData);
        cv.setStatus(CREATED);
    }

    @Transactional
    @Override
    public void updateTemplate(Long id, Long templateId) {
        CV cv = cvRepository.findById(id).orElseThrow();
        boolean isTemplateExists = documentCreatorAdapter.checkTemplateExists(templateId);
        if (!isTemplateExists) {
            throw new TemplateNotFound("No template with name" + templateId);
        }
        cv.setTemplateId(templateId);
        cv.setStatus(FILL_DATA);
    }

    @Transactional
    @Override
    public void changeTemplate(Long id) {
        CV cv = cvRepository.findById(id).orElseThrow();
        cv.setStatus(NEW);
        cv.setTemplateId(null);
    }

    private Document mapFromDto(DocumentDto documentDto) {
        Document document = new Document();
        document.setContent(documentDto.getContent());
        document.setMediaType(documentDto.getMediaType());
        return document;
    }

    private DocumentDto mapToDto(Document document) {
        DocumentDto documentDto = new DocumentDto();
        documentDto.setContent(document.getContent());
        documentDto.setFileName("rezume.pdf");
        documentDto.setMediaType(document.getMediaType());
        return documentDto;
    }

    private CVDto mapToDto(CV cv) {
        CVDto cvDto = new CVDto();
        cvDto.setCreatedDate(cv.getCreatedDate());
        cvDto.setData(cv.getData());
        cvDto.setId(cv.getId());
        cvDto.setTemplateId(cv.getTemplateId());
        cvDto.setStatus(cv.getStatus());
        return cvDto;
    }

    private CVProcessDto mapToProcessDto(CV cv, Map<Long, TemplateInfoDto> templateInfoMap) {
        CVProcessDto cvProcessDto = new CVProcessDto();
        cvProcessDto.setCreatedDate(cv.getCreatedDate());
        cvProcessDto.setData(cv.getData());
        cvProcessDto.setId(cv.getId());
        cvProcessDto.setStatus(cv.getStatus());
        cvProcessDto.setTemplate(templateInfoMap.get(cv.getTemplateId()));
        return cvProcessDto;
    }

    private void assertCVBelongsToUser(UserAccount userAccount, CV cv) {
        if (!userAccount.getId().equals(cv.getUserAccount().getId())) {
           throw new NoAccess();
        }
    }
}
