package com.alex.cvprocess.service;

import com.alex.cvprocess.domain.CV;
import com.alex.cvprocess.domain.UserAccount;
import com.alex.cvprocess.dto.CVDto;
import com.alex.cvprocess.dto.CVProcessDto;
import com.alex.cvprocess.dto.DocumentDto;

import java.util.List;
import java.util.Map;

public interface CVService {
    Long createCV(Long userId);

    CVDto findCVById(long userId, long id);

    List<CVProcessDto> findCVByUserAccount(Long userId);

    DocumentDto getCVDocument(long userAccId, long id);

    void delete(Long id);

    /**
     * Подтверждения данных и создание резюме. Переключает CV в статус CREATED.
     * Выполняется из статуса FILL_DATA
     *
     * @param id   идентификатор CV
     * @param data Данные формы
     */
    void submit(Long id, Map<String, Object> data);

    void updateData(Long id, Map<String, Object> data);

    void updateTemplate(Long id, Long templateId);

    void changeTemplate(Long id);
}
