package com.alex.cvprocess.service;

import com.alex.cvprocess.domain.SecurityRole;
import com.alex.cvprocess.domain.UserAccount;
import com.alex.cvprocess.dto.UserAccountDto;
import com.alex.cvprocess.dto.UserRegistrationDto;
import com.alex.cvprocess.repo.UserAccountRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
@AllArgsConstructor
public class UserAccountServiceImpl implements UserAccountService {
    private final UserAccountRepository userAccountRepository;

    @Override
    public Long addUser(UserRegistrationDto userDto) {
        UserAccount user = new UserAccount();
        user.setEmail(userDto.getEmail());
        user.setUsername(userDto.getUsername());
        user.setPassword(userDto.getPassword());
        user.setIsPremium(false);
        user.setRoles(new String[]{ SecurityRole.USER.getAuthority() });
        return userAccountRepository.save(user).getId();
    }

    @Override
    public UserAccountDto findUserByNameAndPassword(String name, String password) {
        return UserAccountDto.of(userAccountRepository.findUserAccountByUsernameAndPassword(name, password));
    }

    @Override
    public List<UserAccountDto> findAll() {
        return userAccountRepository.findAll().stream().map(UserAccountDto::of).collect(toList());
    }
}
