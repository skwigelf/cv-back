package com.alex.cvprocess.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class IllegalCVStatus extends RuntimeException {

    public IllegalCVStatus() {
    }

    public IllegalCVStatus(String message) {
        super(message);
    }
}
