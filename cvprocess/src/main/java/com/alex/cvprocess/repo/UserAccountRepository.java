package com.alex.cvprocess.repo;

import com.alex.cvprocess.domain.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAccountRepository extends JpaRepository<UserAccount, Long> {
    UserAccount findUserAccountByUsernameAndPassword(String username, String password);
}
