package com.alex.cvprocess.repo;

import com.alex.cvprocess.domain.Document;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DocumentRepository extends JpaRepository<Document, Long> {
}
