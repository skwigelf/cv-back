package com.alex.cvprocess.repo;

import com.alex.cvprocess.domain.CV;
import com.alex.cvprocess.domain.UserAccount;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CVRepository extends JpaRepository<CV, Long> {
    List<CV> findByUserAccount(UserAccount userAccount);
}
