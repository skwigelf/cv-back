package com.alex.cvprocess.config.auth;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
public class PasswordEncodeProviderFactory {

    @Bean
    public PasswordEncoder createEncodeProvider() {
        return new BCryptPasswordEncoder();
    }

}