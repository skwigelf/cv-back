package com.alex.cvprocess.config.auth;

import com.alex.cvprocess.domain.UserAccount;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationProvider {

    private final JwtProvider jwtProvider;

    @Value("${security.system.token}")
    private String systemToken;

    public AuthenticationProvider(JwtProvider jwtProvider) {
        this.jwtProvider = jwtProvider;
    }

    public void authenticate(String token) {
        if (token != null && token.equals(systemToken)) {
            UserAccount userAccount = UserAccount.createSystemUser();
            setSecurityContext(userAccount);
        }
        if (token != null && jwtProvider.validateToken(token)) {
            UserAccount userAccount = jwtProvider.tryGetPrincipalFromToken(token);
            setSecurityContext(userAccount);
        }
    }

    private void setSecurityContext(UserAccount userAccount) {
        UsernamePasswordAuthenticationToken auth = new UsernamePasswordAuthenticationToken(userAccount,
                null, userAccount.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

}