package com.alex.cvprocess.config.auth;

import com.alex.cvprocess.domain.UserAccount;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * This component provides features to validate JWT
 * tokens and extract principal from token payload
 */
@Component
public class JwtProvider {

    /**
     * This secret key is used to encode/decode/validate tokens
     */
    @Value("${security.jwt.secret}")
    private String jwtSecret;

    /**
     * THis method validates whether JWT token is OK
     */
    public boolean validateToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            JWTVerifier verifier = JWT.require(algorithm).build();
            verifier.verify(token);
            return true;
        } catch (Exception e) {
            //todo should be added logging
        }
        return false;
    }

    /**
     * This method decodes JWT token and extracts principal from this one
     */
    @SneakyThrows
    public UserAccount tryGetPrincipalFromToken(String token) {
        String principal = JWT.decode(token).getIssuer();

        return new ObjectMapper()
                .readValue(principal, UserAccount.class);
    }

}