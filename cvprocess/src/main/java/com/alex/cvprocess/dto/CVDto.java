package com.alex.cvprocess.dto;

import com.alex.cvprocess.domain.CVStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CVDto {
    private Long id;
    private CVStatus status;
    private Long templateId;
    private LocalDate createdDate;
    private String data;
}
