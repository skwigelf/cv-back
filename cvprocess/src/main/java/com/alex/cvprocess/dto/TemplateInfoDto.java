package com.alex.cvprocess.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateInfoDto {
    private String mediaType;
    private String filename;
    private String description;
    private String title;
    private byte[] previewImage;
    private Long id;
}
