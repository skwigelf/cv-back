package com.alex.cvprocess.dto;

import com.alex.cvprocess.domain.UserAccount;
import lombok.Value;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

/**
 * This dto holds data of user
 * which have already been persisted
 */
@Value(staticConstructor = "of")
public class UserAccountDto {

    public static UserAccountDto of(UserAccount userAccount) {
        return UserAccountDto.of(userAccount.getId(), userAccount.getUsername(),
                userAccount.getEmail(), userAccount.isEnabled(), userAccount.getAuthorities());
    }

    long id;
    String username;
    String email;
    boolean enabled;
    Collection<? extends GrantedAuthority> authorities;

}