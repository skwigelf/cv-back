package com.alex.cvprocess.dto;


import com.alex.cvprocess.domain.CVStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CVProcessDto {
    private Long id;
    private CVStatus status;
    private TemplateInfoDto template;
    private LocalDate createdDate;
    private String data;
}
