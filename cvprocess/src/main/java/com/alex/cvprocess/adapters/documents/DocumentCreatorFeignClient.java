package com.alex.cvprocess.adapters.documents;

import com.alex.cvprocess.dto.TemplateInfoDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@FeignClient(name = "docs", url = "${service.document}")
@RequestMapping("/api/rest")
public interface DocumentCreatorFeignClient {
    @PostMapping("/docs/{id}")
    ResponseEntity<byte[]> create(@PathVariable Long id, @RequestBody Map<String, Object> data);

    @GetMapping("/docs/{templateId}")
    ResponseEntity<byte[]> getTemplate(@PathVariable Long templateId);

    @PostMapping("/template/meta")
    List<TemplateInfoDto> getTemplatesByIds(@RequestBody List<Long> ids);
}
