package com.alex.cvprocess.adapters.documents;

import com.alex.cvprocess.dto.DocumentDto;
import com.alex.cvprocess.dto.TemplateInfoDto;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
@AllArgsConstructor
public class DocumentCreatorAdapter {
    private final DocumentCreatorFeignClient feignClient;

    public DocumentDto createCV(Long id, Map<String, Object> attributes) {
        ResponseEntity<byte[]> response = feignClient.create(id, attributes);
        HttpHeaders headers = response.getHeaders();

        return new DocumentDto(headers.getContentType().toString(), response.getBody(), headers.getContentDisposition().getFilename());
    }

    public List<TemplateInfoDto> getTemplatesByIds(List<Long> ids) {
        return feignClient.getTemplatesByIds(ids);
    }

    public boolean checkTemplateExists(Long templateId) {
        ResponseEntity<byte[]> response = feignClient.getTemplate(templateId);
        return response.getStatusCode() == HttpStatus.OK;
    }
}
