package com.alex.cvprocess.domain;

import com.alex.cvprocess.util.CustomAuthorityDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.stream.Collectors;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserAccount implements UserDetails, Serializable {
    private final static String ROLE_DELIMITER = ",";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String email;
    private String password;
    private Boolean isPremium;
    private String roles;
    private boolean enabled;

    @Transient
    private boolean accountNonExpired;
    @Transient
    private boolean accountNonLocked;
    @Transient
    private boolean credentialsNonExpired;

    public static UserAccount createSystemUser() {
        return UserAccount.builder()
                .id(0L)
                .username("system")
                .password(null)
                .roles(SecurityRole.SYSTEM.getAuthority())
                .email(null)
                .enabled(true)
                .build();
    }

    public void setRoles(String[] roles) {
        this.roles = String.join(ROLE_DELIMITER, roles);
    }

    @Override
    @JsonDeserialize(using = CustomAuthorityDeserializer.class)
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return StringUtils.isEmpty(roles)
                ? Collections.emptyList()
                : Arrays.stream(roles.split(ROLE_DELIMITER))
                .map(SecurityRole::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
