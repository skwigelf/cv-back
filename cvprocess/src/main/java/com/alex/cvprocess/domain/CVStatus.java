package com.alex.cvprocess.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum CVStatus {
    NEW("New"),
    FILL_DATA("FillData"),
    CREATED("Created");

    String status;
}
