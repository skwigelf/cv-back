package com.alex.cvprocess.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

/**
 * This enum holds user roles
 */
@Getter
@AllArgsConstructor
public enum SecurityRole implements GrantedAuthority {

    /**
     * This role belongs to Jhack platform clients
     */
    USER("USER"),

    /**
     * This role belongs to super users
     */
    ADMIN("ADMIN"),

    /**
     * This role belongs to another microservices
     */
    SYSTEM("SYSTEM");

    private String authority;

}
