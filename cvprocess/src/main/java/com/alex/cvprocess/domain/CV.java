package com.alex.cvprocess.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CV {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private CVStatus status;
    private Long templateId;
    private LocalDate createdDate;
    private String data;
    @JoinColumn(name = "user_account_id")
    @ManyToOne(cascade = CascadeType.ALL)
    private UserAccount userAccount;
    @JoinColumn(name = "document_id")
    @OneToOne(cascade = CascadeType.ALL)
    private Document document;
}
