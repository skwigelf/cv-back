create table document
(
    id        bigserial primary key not null,
    media_type varchar(256)          not null,
    content   bytea
);