create table cv
(
    id              bigserial primary key not null,
    status          varchar(50)           not null,
    template_id     bigint,
    created_date    date,
    data            text,
    document_id     bigint,
    user_account_id bigint
);