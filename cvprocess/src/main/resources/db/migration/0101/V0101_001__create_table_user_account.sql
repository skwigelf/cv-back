create table user_account
(
    id         bigserial primary key not null,
    username   varchar(200)          not null,
    email      varchar(200)          not null,
    password   text,
    is_premium boolean               not null,
    roles      varchar(128)          not null,
    enabled    boolean               not null

);