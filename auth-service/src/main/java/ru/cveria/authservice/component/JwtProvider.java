package ru.cveria.authservice.component;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.cveria.authservice.domain.model.UserAccount;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

/**
 * This component provides features for generating JWT tokens for principals
 */
@Component
public class JwtProvider {

    @Value("${jwt.secret}")
    private String jwtSecret;

    public String generateToken(UserAccount userAccount) {
        Date date = Date.from(LocalDate.now().plusDays(15).atStartOfDay(ZoneId.systemDefault()).toInstant());
        String stringifyedPrincipal = stringifyPrincipal(userAccount);
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            return JWT.create()
                    .withIssuer(stringifyedPrincipal)
                    .withExpiresAt(date)
                    .sign(algorithm);
        } catch (Exception e) {
            throw new RuntimeException("Could not generate token", e);
        }
    }

    @SneakyThrows
    private String stringifyPrincipal(UserAccount userAccount) {
        return new ObjectMapper().writeValueAsString(userAccount);
    }

}