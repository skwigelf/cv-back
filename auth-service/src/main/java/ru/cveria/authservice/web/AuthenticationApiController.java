package ru.cveria.authservice.web;

import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.cveria.authservice.domain.dto.AuthOperationInfoHolder;
import ru.cveria.authservice.domain.service.AuthenticationService;
import ru.cveria.authservice.domain.type.AuthStatus;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class AuthenticationApiController {

    private final AuthenticationService authenticationService;

    @ApiOperation("Log-in user")
    @PostMapping("/login")
    public HttpEntity<TokenHolder> login(@RequestBody CredentialsHolder credentials) {
        AuthOperationInfoHolder authOperationInfoHolder =
                authenticationService.authenticate(credentials.getUsername(), credentials.getPassword());
        if (authOperationInfoHolder.getStatus() == AuthStatus.SUCCESS) {
            String token = authOperationInfoHolder.getToken();
            return ResponseEntity.ok(new TokenHolder(token));
        } else {
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }
    }

    @Data
    @AllArgsConstructor
    static class TokenHolder {
        private String token;
    }

    @Data
    @AllArgsConstructor
    static class CredentialsHolder {
        private String username;
        private String password;
    }

}