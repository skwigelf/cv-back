package ru.cveria.authservice.domain.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.cveria.authservice.client.UserServiceClient;
import ru.cveria.authservice.component.JwtProvider;
import ru.cveria.authservice.domain.dto.AuthOperationInfoHolder;
import ru.cveria.authservice.domain.model.UserAccount;

@Service
public class AuthenticationService {

    @Value("${jwt.system.token}")
    private String token;

    private final UserServiceClient userServiceClient;
    private final JwtProvider jwtProvider;

    public AuthenticationService(UserServiceClient userServiceClient, JwtProvider jwtProvider) {
        this.userServiceClient = userServiceClient;
        this.jwtProvider = jwtProvider;
    }

    public AuthOperationInfoHolder authenticate(String username, String password) {
        UserAccount userAccount = userServiceClient
                .tryFetchUserAccountByNameAndPassword(token, username, password);
        if (userAccount == null) {
            return AuthOperationInfoHolder.createFailed();
        }
        String token = jwtProvider.generateToken(userAccount);
        return AuthOperationInfoHolder.createSuccessful(token);
    }

}