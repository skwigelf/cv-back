package ru.cveria.authservice.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.cveria.authservice.domain.type.SecurityRole;

import java.io.Serializable;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * This entity represents user of platform
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAccount implements UserDetails, Serializable {

    private final static String ROLE_DELIMITER = ",";

    private long id;

    private String username;

    private String password;

    private String email;

    private boolean enabled;

    Collection<String> authorities;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities.stream()
                .map(SecurityRole::valueOf)
                .collect(Collectors.toList());
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }
}