package ru.cveria.authservice.domain.dto;

import lombok.Value;
import ru.cveria.authservice.domain.type.AuthStatus;

@Value
public class AuthOperationInfoHolder {

    public static AuthOperationInfoHolder createFailed() {
        return new AuthOperationInfoHolder(null, AuthStatus.FAILED);
    }

    public static AuthOperationInfoHolder createSuccessful(String token) {
        return new AuthOperationInfoHolder(token, AuthStatus.SUCCESS);
    }

    String token;
    AuthStatus status;

}