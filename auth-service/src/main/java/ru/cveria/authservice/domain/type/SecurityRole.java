package ru.cveria.authservice.domain.type;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

@Getter
@AllArgsConstructor
public enum SecurityRole implements GrantedAuthority {

    USER("USER"),
    ADMIN("ADMIN");

    private String authority;

}