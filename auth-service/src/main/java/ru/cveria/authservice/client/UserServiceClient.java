package ru.cveria.authservice.client;

import feign.Headers;
import feign.Param;
import feign.RequestLine;
import ru.cveria.authservice.domain.model.UserAccount;

/**
 * This client sends requests to user service
 */
public interface UserServiceClient {

    @RequestLine("GET /api/users/func/try-find-by-username-and-password?username={username}&password={password}")
    @Headers("Authorization: Bearer {token}")
    UserAccount tryFetchUserAccountByNameAndPassword(@Param("token") String token,
                                                     @Param("username") String name,
                                                     @Param("password") String password);

}
