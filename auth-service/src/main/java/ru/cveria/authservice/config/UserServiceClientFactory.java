package ru.cveria.authservice.config;

import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.cveria.authservice.client.UserServiceClient;

/**
 * This factory setups user service client bean. See {@link UserServiceClient}
 */
@Configuration
public class UserServiceClientFactory {

    @Value("${user.service.host}")
    private String userServiceHost;

    @Bean
    public UserServiceClient createClient() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(UserServiceClient.class, userServiceHost);

    }

}